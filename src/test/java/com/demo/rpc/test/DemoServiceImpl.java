package com.demo.rpc.test;

import java.time.Instant;

public class DemoServiceImpl implements DemoService {
    public String sayHello(String param) {
        System.out.println(Instant.now() + ", param" + param);
        return "hello:" + param;
    }
}